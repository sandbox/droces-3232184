<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_results_answers",
 *   source_module = "quiz"
 * )
 */
class QuizResultsAnswers extends DrupalSqlBase {

  /**
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  public function query() {
    $query = $this->select('quiz_node_results_answers', 'ra')
      ->fields('ra', [
        'result_id',
        'question_nid',
        'question_vid',
        'is_correct',
        'is_skipped',
        'points_awarded',
        'answer_timestamp',
        'number',
        'is_doubtful',
      ])
      ->condition('answer_timestamp', strtotime('2020-09-01'), '>')
      ->orderBy('ra.number', 'ASC');
    return $query;
  }

  /**
   * @param \Drupal\migrate\Row $row
   *
   * @return bool
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    $result_id = $row->getSourceProperty('result_id');
    $query = $this->select('quiz_multichoice_user_answers', 'user_answers')
      ->fields('multi', ['answer_id'])
      ->condition('result_id', $result_id);
    $query->join('quiz_multichoice_user_answer_multi', 'multi', 'user_answers.id = multi.user_answer_id');
    $results = $query->execute()->fetchAllAssoc('answer_id');
    if (!empty($results)) {
      $result = reset($results);
      $row->setSourceProperty('answer_id', $result['answer_id']);
    }
    return parent::prepareRow($row);
  }


  /**
   * @return \string[][]
   */
  public function getIds() {
    return [
      'result_id' => [
        'type' => 'integer',
        'alias' => 'ra',
      ],
      'question_nid' => [
        'type' => 'integer',
        'alias' => 'ra',
      ],
    ];
  }

  /**
   * @return array
   */
  public function fields() {
    return [
      'result_id' => $this->t('The result ID'),
      'question_nid' => $this->t('The question id'),
      'question_vid' => $this->t('The question vid'),
      'is_correct' => $this->t('is_correct'),
      'is_skipped' => $this->t('is skipped'),
      'points_awarded' => $this->t('Points awarded'),
      'answer_timestamp' => $this->t('Timestamp'),
      'number' => $this->t('Number'),
      'is_doubtful' => $this->t('Is Doubtful'),
      'answer_id' => $this->t('Answer ID'),
    ];
  }

}
