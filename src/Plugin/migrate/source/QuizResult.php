<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "d7_quiz_result",
 *   core = {7},
 *   source_module = "quiz"
 * )
 */
class QuizResult extends DrupalSqlBase {

  public function query() {
    $query = $this->select('quiz_node_results', 'nr')
      ->fields('nr', [
        'result_id',
        'nid',
        'vid',
        'uid',
        'time_start',
        'time_end',
        'released',
        'score',
        'is_invalid',
        'is_evaluated',
        'attempt',
        'type',
      ])
      ->orderBy('nr.result_id', 'DESC');

    return $query;
  }

  public function getIds() {
    return [
      'result_id' => [
        'type' => 'integer',
        'alias' => 'nr',
      ],
    ];
  }

  public function fields() {
    $fields = [
      'result_id' => $this->t('The result ID'),
      'nid' => $this->t('The quiz id'),
      'vid' => $this->t('The quiz vid'),
      'uid' => $this->t('Who took the quiz'),
      'time_start' => $this->t('When the quiz was started'),
      'time_end' => $this->t('When the quiz was finished'),
      'released' => $this->t('Released'),
      'score' => $this->t('Score'),
      'is_invalid' => $this->t('Invalid'),
      'is_evaluated' => $this->t('Evaluated'),
      'attempt' => $this->t('Attempt'),
      'type' => $this->t('Result type'),
    ];

    return $fields;
  }

}
