<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question_multichoice",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionMultichoice extends D7_node {

  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->addMultichoiceProps($row, $nid);
      $this->addQuizProps($row, $nid);
      $this->addAnswers($row, $nid);
    }

    return parent::prepareRow($row);
  }

  protected function addMultichoiceProps(Row $row, $nid) {
    $query = $this->select('quiz_multichoice_properties', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'choice_multi',
        'choice_random',
        'choice_boolean',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

  protected function addQuizProps(Row $row, $nid) {
    $query = $this->select('quiz_node_properties', 'qn')
      ->fields('qn', [
        'max_score',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qn.nid', $nid);

    $results = $query->execute()->fetchCol();

    if (!empty($results)) {
      $result = reset($results);

      $row->setSourceProperty('max_score', $result);
    }
  }

  protected function addAnswers(Row $row, $nid) {
    $vid = $row->getSourceProperty('vid');

    $query = $this->select('quiz_multichoice_answers', 'am')
      ->fields('am', [
        'id',
      ])
      ->orderBy('id', 'ASC')
      ->condition('am.question_nid', $nid)
      ->condition('am.question_vid', $vid);

    $results = $query->execute()->fetchCol();

    if (!empty($results)) {
//      $answers = array_map(function ($value) {
//          return [$value];
//        }, $results);

      $row->setSourceProperty('answers', $results);
    }
  }

}
