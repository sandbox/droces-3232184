<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "d7_quiz",
 *   core = {7},
 *   source_module = "quiz"
 * )
 */
class Quiz extends D7_node {

  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $query = $this->select('quiz_node_properties', 'qp')
        ->fields('qp', [
          'vid',
          'nid',
          'number_of_random_questions',
          'max_score_for_random',
          'pass_rate',
          'summary_pass',
          'summary_pass_format',
          'summary_default',
          'summary_default_format',
          'randomization',
          'backwards_navigation',
          'keep_results',
          'repeat_until_correct',
          'quiz_open',
          'quiz_close',
          'takes',
          'show_attempt_stats',
          'time_limit',
          'quiz_always',
          'time_left',
          'max_score',
          'allow_skipping',
          'allow_resume',
          'allow_jumping',
          'show_passed',
          'mark_doubtful',
          'allow_change',
        ])
        ->condition('qp.nid', $nid)
        ->orderBy('qp.vid','DESC');

      $results = $query->execute()->fetchAllAssoc('vid');

      if (!empty($results)) {
        $result = reset($results);

        foreach ($result as $key => $value) {
          $row->setSourceProperty($key, $value);
        }
      }

      $options_query = $this->select('quiz_node_result_options', 'ro');
      $options_query->fields('ro', ['option_id']);
      $options_query->condition('nid', $nid);
      $options_query->condition('vid', $row->getSourceProperty('vid'));
      $options_results = $options_query->execute();

      $result_options = [];
      if (!empty($options_results)) {
        foreach ($options_results as $options_result) {
          $result_options[] = $options_result['option_id'];
        }
      }

      $row->setSourceProperty('result_options', $result_options);


    }

    return parent::prepareRow($row);
  }

}
