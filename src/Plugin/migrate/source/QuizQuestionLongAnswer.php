<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question_long_answer",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionLongAnswer extends D7_node {

  /**
   * PrepareRow().
   */
  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->AddProperties($row, $nid);
      $this->AddFeedbackProperties($row, $nid);
    }

    return parent::prepareRow($row);
  }

  /**
   * Gets the results from quiz_long_answer_node_properties table.
   */
  protected function addProperties(Row $row, $nid) {
    $query = $this->select('quiz_long_answer_node_properties', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'rubric',
        'rubric_format',
        'answer_text_processing',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

  /**
   * Gets the results from quiz_question_properties table.
   */
  protected function addFeedbackProperties(Row $row, $nid) {
    $query = $this->select('quiz_question_properties', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'max_score',
        'feedback',
        'feedback_format',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

}
