<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\paragraphs\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_result_options",
 *   source_module = "quiz"
 * )
 */
class QuizResultOptions extends DrupalSqlBase {

  public function fields() {
    return [
      'option_id' => $this->t('The option ID'),
      'nid' => $this->t('The question id'),
      'vid' => $this->t('The question vid'),
      'option_name' => $this->t('The name of the option'),
      'option_summary' => $this->t('The summary of the option'),
      'option_summary_format' => $this->t('The format of the summary of the option'),
      'option_start' => $this->t('The start of the option'),
      'option_end' => $this->t('The end of the option'),
    ];
  }

  public function getIds() {
    return [
      'option_id' => [
        'type' => 'integer',
        'alias' => 'ro',
      ],
    ];
  }

  public function query() {
    $query = $this->select('quiz_node_result_options', 'ro')
      ->fields('ro', [
        'option_id',
        'nid',
        'vid',
        'option_name',
        'option_summary',
        'option_summary_format',
        'option_start',
        'option_end',
      ])
      ->orderBy('ro.option_id', 'ASC');
    return $query;
  }

}

