<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\paragraphs\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_result_types",
 *   source_module = "quiz"
 * )
 */
class QuizResultTypes extends DrupalSqlBase {

  public function fields() {
    return [
      'id' => 'Unique result type ID.',
      'type' => 'The machine-readable name of this result type.',
      'label' => 'The human-readable name of this result type.',
      'status' => 'The exportable status of the entity.',
      'module' => 'The name of the providing module if the entity has been defined in code.',
    ];
  }

  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'rt',
      ],
    ];
  }

  public function query() {
    $query = $this->select('quiz_result_type', 'rt')
      ->fields('rt', [
        'id',
        'type',
        'label',
        'status',
        'module',
      ]);
    return $query;
  }

}

