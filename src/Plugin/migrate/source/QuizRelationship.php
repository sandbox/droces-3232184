<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_relationship",
 *   source_module = "quiz"
 * )
 */
class QuizRelationship extends DrupalSqlBase {

  public function query() {
    $query = $this->select('node', 'n')
      ->fields('n', [
        'nid',
        'vid',
      ])
      ->condition('n.type', 'quiz');

    $query->leftJoin('quiz_node_relationship', 'qr', 'n.nid = qr.parent_nid AND n.vid = qr.parent_vid');

    $query->fields('qr', [
      'child_nid',
      'child_vid',
      'question_status',
      'weight',
      'max_score',
      'auto_update_max_score',
    ])
    ->orderBy('n.nid', 'ASC');

    return $query;
  }

  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
        'alias' => 'n',
      ],
      'child_nid' => [
        'type' => 'integer',
        'alias' => 'qr',
      ],
    ];
  }

  public function fields() {
    $fields = [
      'nid' => $this->t('The quiz nid'),
      'vid' => $this->t('The quiz vid'),
      'child_nid' => $this->t('The question nid'),
      'child_vid' => $this->t('The question vid'),
      'question_status' => $this->t('The question status'),
      'weight' => $this->t('The question weight'),
      'max_score' => $this->t('The question max score'),
      'auto_update_max_score' => $this->t('The question auto update max score'),
    ];

    return $fields;
  }

}
