<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 quiz question t/f module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question_truefalse",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionTrueFalse extends D7_node {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->AddAnswer($row, $nid);
      $this->AddProperties($row, $nid);
    }

    return parent::prepareRow($row);
  }

  /**
   * Gets quiz_truefalse_node table.
   */
  protected function addAnswer(Row $row, $nid) {
    $query = $this->select('quiz_truefalse_node', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'correct_answer',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

  /**
   * Gets the results from quiz_question_properties table.
   */
  protected function addProperties(Row $row, $nid) {
    $query = $this->select('quiz_question_properties', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'max_score',
        'feedback',
        'feedback_format',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

}
