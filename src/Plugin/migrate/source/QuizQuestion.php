<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7_node;

/**
 * Drupal 7 quiz question source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question",
 *   source_module = "quiz"
 * )
 */
class QuizQuestion extends D7_node {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->AddProperties($row, $nid);
    }
    return parent::prepareRow($row);
  }

  /**
   * Gets the results from quiz_question_properties table.
   */
  protected function addProperties(Row $row, $nid) {
    $query = $this->select('quiz_question_properties', 'qp')
      ->fields('qp', [
        'vid',
        'nid',
        'max_score',
        'feedback',
        'feedback_format',
      ])
      ->orderBy('vid', 'DESC')
      ->condition('qp.nid', $nid);

    $results = $query->execute()->fetchAllAssoc('vid');

    if (!empty($results)) {
      $result = reset($results);

      foreach ($result as $key => $value) {
        $row->setSourceProperty($key, $value);
      }
    }
  }

}
