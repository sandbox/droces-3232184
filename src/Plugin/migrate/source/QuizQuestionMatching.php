<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_question_matching",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionMatching extends QuizQuestion {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->hasSourceProperty('nid')) {
      $nid = $row->getSourceProperty('nid');

      $this->AddMatches($row, $nid);
    }

    return parent::prepareRow($row);
  }

  /**
   * Add matches from table quiz_matching_node.
   */
  protected function addMatches(Row $row, $nid) {
    $vid = $row->getSourceProperty('vid');

    $query = $this->select('quiz_matching_node', 'mn')
      ->fields('mn', [
        'match_id',
        'question',
        'answer',
        'feedback',
      ])
      ->orderBy('match_id', 'ASC')
      ->condition('mn.nid', $nid)
      ->condition('mn.vid', $vid);

    $results = $query->execute()->fetchCol();

    $row->setSourceProperty('matches', $results);
  }

}
