<?php

namespace Drupal\quiz_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 module source from database.
 *
 * @MigrateSource(
 *   id = "quiz_matching_answer",
 *   source_module = "quiz"
 * )
 */
class QuizQuestionMatchingAnswer extends DrupalSqlBase {

  /**
   * Implements query().
   */
  public function query() {
    $query = $this->select('quiz_matching_node', 'mn')
      ->fields('mn', [
        'match_id',
        'nid',
        'vid',
        'question',
        'answer',
        'feedback',
      ])
      ->orderBy('mn.match_id');

    return $query;
  }

  /**
   * Implements getIds().
   */
  public function getIds() {
    return [
      'match_id' => [
        'type' => 'integer',
        'alias' => 'mn',
      ],
    ];
  }

  /**
   * Implements fields().
   */
  public function fields() {
    $fields = [
      'match_id' => $this->t('Match id'),
      'nid' => $this->t('Node id'),
      'vid' => $this->t('Vid'),
      'question' => $this->t('Question'),
      'answer' => $this->t('Answer'),
      'feedback' => $this->t('Feedback'),
    ];

    return $fields;
  }

  /**
   * Implements prepareRow().
   */
  public function prepareRow(Row $row) {
    $answer = [
      0 => [
        'value' => $row->getSourceProperty('answer'),
      ],
    ];
    $row->setSourceProperty('answer', $answer);
    $question = [
      0 => [
        'value' => $row->getSourceProperty('question'),
      ],
    ];
    $row->setSourceProperty('question', $question);
    $feedback = [
      0 => [
        'value' => $row->getSourceProperty('feedback'),
      ],
    ];
    $row->setSourceProperty('feedback', $feedback);
    return parent::prepareRow($row);
  }

}
